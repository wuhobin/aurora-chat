package com.chat.aurorachat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuroraChatApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuroraChatApplication.class, args);
    }

}
