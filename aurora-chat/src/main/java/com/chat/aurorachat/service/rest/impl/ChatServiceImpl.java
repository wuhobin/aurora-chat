package com.chat.aurorachat.service.rest.impl;

import com.chat.aurorachat.config.properties.OpenAiProperties;
import com.chat.aurorachat.service.rest.ChatService;
import com.theokanning.openai.completion.chat.ChatCompletionRequest;
import com.theokanning.openai.completion.chat.ChatCompletionResult;
import com.theokanning.openai.completion.chat.ChatMessage;
import com.theokanning.openai.service.OpenAiService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wuhongbin
 * @ClassName ChatServiceImpl
 * @description: TODO
 * @date 2024年03月26日
 * @version: 1.0.0
 */
@Service
@Slf4j
public class ChatServiceImpl implements ChatService {

    @Autowired
    private OpenAiService openAiService;

    @Autowired
    private OpenAiProperties openAiProperties;

    @Override
    public String chat(String prompt) {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setRole("system");
        chatMessage.setContent("你的名字叫AuroraAI,你是Aurora开源项目的AI聊天机器人，你的创造者是whb以及其他贡献者。当有人问你问题时你只能回答500字以内");

        ChatMessage userMessage = new ChatMessage();
        userMessage.setRole("user");
        userMessage.setContent(prompt);
        List<ChatMessage> messages = new ArrayList<ChatMessage>();
        messages.add(chatMessage);
        messages.add(userMessage);

        ChatCompletionRequest completionRequest = ChatCompletionRequest.builder()
                .messages(messages)
                .model(openAiProperties.getModel())
                .temperature(1.0)
                .topP(0.9)
                .frequencyPenalty(0.0)
                .presencePenalty(0.6)
                .maxTokens(openAiProperties.getMaxTokens())
                .build();
        ChatCompletionResult chatCompletion = openAiService.createChatCompletion(completionRequest);
        if (ObjectUtils.isEmpty(chatCompletion) || ObjectUtils.isEmpty(chatCompletion.getChoices()) || ObjectUtils.isEmpty(chatCompletion.getChoices().get(0))){
            return "抱歉，我无法回答你的问题，请稍后再试。如果问题仍然存在，请尝试其他问题或联系项目维护者。";
        }
        String reply = chatCompletion.getChoices().get(0).getMessage().getContent();
        log.info("Chat completion result: {}", chatCompletion);
        return reply;
    }
}
