package com.chat.aurorachat.service;

import com.chat.aurorachat.dataobject.UserDO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wuhongbin
 * @since 2024-03-21
 */
public interface UserService extends IService<UserDO> {

}
