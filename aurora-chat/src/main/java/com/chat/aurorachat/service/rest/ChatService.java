package com.chat.aurorachat.service.rest;

/**
 * chatGpt service
 * @author wuhongbin
 * @ClassName ChatService
 * @description: TODO
 * @date 2024年03月26日
 * @version: 1.0.0
 */
public interface ChatService {

    String chat(String prompt);

}
