package com.chat.aurorachat.controller;

import com.chat.aurorachat.service.rest.ChatService;
import com.chat.aurorachat.utils.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wuhongbin
 * @ClassName ChatController
 * @description: TODO
 * @date 2024年03月20日
 * @version: 1.0.0
 */
@RestController
@RequestMapping("/chat")
public class ChatController {

    @Autowired
    private ChatService chatService;

    @GetMapping("/completion")
    public CommonResult completion(String prompt) {
        String chat = chatService.chat(prompt);
        return CommonResult.success(chat);
    }
}
