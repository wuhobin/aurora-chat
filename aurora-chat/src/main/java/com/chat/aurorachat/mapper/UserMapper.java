package com.chat.aurorachat.mapper;

import com.chat.aurorachat.dataobject.UserDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wuhongbin
 * @since 2024-03-21
 */
public interface UserMapper extends BaseMapper<UserDO> {

}
