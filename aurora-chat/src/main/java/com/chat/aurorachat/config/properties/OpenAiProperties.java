package com.chat.aurorachat.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author wuhongbin
 * @ClassName OpenAiProperties
 * @description: TODO
 * @date 2024年03月27日
 * @version: 1.0.0
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "api")
public class OpenAiProperties {

    private String chatGptKey;

    private String proxyHost;

    private Integer proxyPort;

    private String model;

    private Integer maxTokens;

}
