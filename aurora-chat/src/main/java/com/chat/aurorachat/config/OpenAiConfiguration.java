package com.chat.aurorachat.config;

import com.chat.aurorachat.config.properties.OpenAiProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.theokanning.openai.client.OpenAiApi;
import com.theokanning.openai.service.OpenAiService;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.time.Duration;

import static com.theokanning.openai.service.OpenAiService.*;

/**
 * @author wuhobin
 * openai chatGpt配置类
 */
@Configuration
public class OpenAiConfiguration {
 
    @Autowired
    private OpenAiProperties openAiProperties;

    /**
     * 需要本地开启vpn代理，代理设置代理地址以及端口，才可以访问openAi接口
     * chatGpt service
     * @return OpenAiService
     */
    @Bean
    public OpenAiService openAiService(){
        ObjectMapper mapper = defaultObjectMapper();
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(openAiProperties.getProxyHost(), openAiProperties.getProxyPort()));
        OkHttpClient client =  defaultClient(openAiProperties.getChatGptKey(), Duration.ofSeconds(10000))
                .newBuilder()
                .proxy(proxy)
                .build();
        Retrofit retrofit = defaultRetrofit(client, mapper);
        OpenAiApi api = retrofit.create(OpenAiApi.class);
        return new OpenAiService(api);
    }
}