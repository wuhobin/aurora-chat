package com.chat.aurorachat;

import com.chat.aurorachat.service.rest.ChatService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AuroraChatApplicationTests {


    @Autowired
    private ChatService chatService;

    @Test
    public void chat(){
        String chat = chatService.chat("你是谁");
        System.out.println(chat);
    }

}
